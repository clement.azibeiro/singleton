#include "classeA.h"
#include <iostream>

using namespace std;

int main()
{
  //Création d'une instance de la classe 'classeA'
  //classeA objet = classeA();
  //Utilisation de la méthode 'getInstance()' afin de créer un singleton
  classeA *premier = classeA::getInstance();
  classeA *second = classeA::getInstance();
  //Affichage de l'attribut 'nombre' de l'instance de 'classeA'
  cout << second->nombre << endl;
  //Affichage de l'adresse des pointeurs
  cout << "Adresse de premier : " << premier << endl;
  cout << "Adresse de second : " << second << endl;
  cout << "L'instance de la classe 'classeA' est bien singulière" << endl;
  return 0;
}
