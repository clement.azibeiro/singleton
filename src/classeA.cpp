#include "classeA.h"

classeA::classeA()
{
  nombre = 123;
}

classeA *classeA::instance = 0;

classeA *classeA::getInstance()
{
  if(instance == 0)
  {
    instance = new classeA();
  }

  return instance;
}
